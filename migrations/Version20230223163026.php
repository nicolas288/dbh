<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230223163026 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE ticket (id INT AUTO_INCREMENT NOT NULL, customer_product_id INT NOT NULL, mantainer_id INT DEFAULT NULL, title VARCHAR(255) NOT NULL, description LONGTEXT NOT NULL, status INT NOT NULL, photo VARCHAR(255) DEFAULT NULL, final_date DATE NOT NULL, action_date DATE NOT NULL, INDEX IDX_97A0ADA3B84D62D8 (customer_product_id), INDEX IDX_97A0ADA37660F5A (mantainer_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE ticket ADD CONSTRAINT FK_97A0ADA3B84D62D8 FOREIGN KEY (customer_product_id) REFERENCES customer_product (id)');
        $this->addSql('ALTER TABLE ticket ADD CONSTRAINT FK_97A0ADA37660F5A FOREIGN KEY (mantainer_id) REFERENCES `user` (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE ticket DROP FOREIGN KEY FK_97A0ADA3B84D62D8');
        $this->addSql('ALTER TABLE ticket DROP FOREIGN KEY FK_97A0ADA37660F5A');
        $this->addSql('DROP TABLE ticket');
    }
}
