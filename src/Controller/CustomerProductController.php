<?php

namespace App\Controller;

use App\Entity\CustomerProduct;
use App\Form\CustomerProductType;
use App\Repository\CustomerProductRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/customer/product')]
class CustomerProductController extends AbstractController
{
    #[Route('/', name: 'app_customer_product_index', methods: ['GET'])]
    public function index(CustomerProductRepository $customerProductRepository): Response
    {
        return $this->render('customer_product/index.html.twig', [
            'customer_products' => $customerProductRepository->findAll(),
        ]);
    }

    #[Route('/new', name: 'app_customer_product_new', methods: ['GET', 'POST'])]
    public function new(Request $request, CustomerProductRepository $customerProductRepository): Response
    {
        $customerProduct = new CustomerProduct();
        $form = $this->createForm(CustomerProductType::class, $customerProduct);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $customerProductRepository->save($customerProduct, true);

            return $this->redirectToRoute('app_customer_product_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('customer_product/new.html.twig', [
            'customer_product' => $customerProduct,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_customer_product_show', methods: ['GET'])]
    public function show(CustomerProduct $customerProduct): Response
    {
        return $this->render('customer_product/show.html.twig', [
            'customer_product' => $customerProduct,
        ]);
    }

    #[Route('/{id}/edit', name: 'app_customer_product_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, CustomerProduct $customerProduct, CustomerProductRepository $customerProductRepository): Response
    {
        $form = $this->createForm(CustomerProductType::class, $customerProduct);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $customerProductRepository->save($customerProduct, true);

            return $this->redirectToRoute('app_customer_product_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('customer_product/edit.html.twig', [
            'customer_product' => $customerProduct,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_customer_product_delete', methods: ['POST'])]
    public function delete(Request $request, CustomerProduct $customerProduct, CustomerProductRepository $customerProductRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$customerProduct->getId(), $request->request->get('_token'))) {
            $customerProductRepository->remove($customerProduct, true);
        }

        return $this->redirectToRoute('app_customer_product_index', [], Response::HTTP_SEE_OTHER);
    }
}
