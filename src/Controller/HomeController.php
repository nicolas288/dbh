<?php

namespace App\Controller;

use App\Repository\CustomerProductRepository;
use App\Repository\ProductRepository;
use App\Repository\TicketRepository;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

#[IsGranted('ROLE_USER')]
class HomeController extends AbstractController
{
    #[Route('/', name:'app_dispatch')]
    public function dispatch() {
        $user = $this->getUser();
        if (in_array('ROLE_ADMIN', $user->getRoles())) {
            return $this->redirectToRoute('index_admin');
        } else if (in_array('ROLE_CUSTOMER', $user->getRoles())) {
            return $this->redirectToRoute('index_customer');
        } else if (in_array('ROLE_MANTAINER', $user->getRoles())) {
            return $this->redirectToRoute('');
        }
        return $this->render('no-access.html.twig');
    }

    #[IsGranted('ROLE_ADMIN')]
    #[Route('/admin', name:'index_admin')]
    public function indexAdmin(UserRepository $userRepository, TicketRepository $ticketRepository, ProductRepository $productRepository) {
        $tickets = $ticketRepository->findAll();
        $customers = $userRepository->getCustomers();
        $mantainers = $userRepository->getMantainers();
        $products = $productRepository->findAll();
        return $this->render('admin/index.html.twig',
        [
            'tickets' => $tickets,
            'customers' => $customers,
            'mantainers' => $mantainers,
            'products' => $products
        ]);
    }

    #[IsGranted('ROLE_CUSTOMER')]
    #[Route('/customer', name:'index_customer')]
    public function indexCustomer(TicketRepository $ticketRepository, CustomerProductRepository $customerProductRepository) {
        $tickets = $ticketRepository->getTicketsByCustomer($this->getUser());
        $products = $customerProductRepository->findBy(['user' => $this->getUser()]);
        return $this->render('customer/index.html.twig',
            [
                'tickets' => $tickets,
                'products' => $products
            ]);
    }
}
