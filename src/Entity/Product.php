<?php

namespace App\Entity;

use App\Repository\ProductRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: ProductRepository::class)]
class Product
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $name = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $picture = null;

    #[ORM\Column(length: 255)]
    private ?string $brand = null;

    #[ORM\Column(length: 255)]
    private ?string $model = null;

    #[ORM\Column(length: 255)]
    private ?string $subGamme = null;

    #[ORM\Column(length: 255)]
    private ?string $type = null;

    #[ORM\OneToMany(mappedBy: 'product', targetEntity: CustomerProduct::class)]
    private Collection $customerProducts;

    public function __construct()
    {
        $this->customerProducts = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getPicture(): ?string
    {
        return $this->picture;
    }

    public function setPicture(?string $picture): self
    {
        $this->picture = $picture;

        return $this;
    }

    public function getBrand(): ?string
    {
        return $this->brand;
    }

    public function setBrand(string $brand): self
    {
        $this->brand = $brand;

        return $this;
    }

    public function getModel(): ?string
    {
        return $this->model;
    }

    public function setModel(string $model): self
    {
        $this->model = $model;

        return $this;
    }

    public function getSubGamme(): ?string
    {
        return $this->subGamme;
    }

    public function setSubGamme(string $subGamme): self
    {
        $this->subGamme = $subGamme;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return Collection<int, CustomerProduct>
     */
    public function getCustomerProducts(): Collection
    {
        return $this->customerProducts;
    }

    public function addCustomerProduct(CustomerProduct $customerProduct): self
    {
        if (!$this->customerProducts->contains($customerProduct)) {
            $this->customerProducts->add($customerProduct);
            $customerProduct->setProduct($this);
        }

        return $this;
    }

    public function removeCustomerProduct(CustomerProduct $customerProduct): self
    {
        if ($this->customerProducts->removeElement($customerProduct)) {
            // set the owning side to null (unless already changed)
            if ($customerProduct->getProduct() === $this) {
                $customerProduct->setProduct(null);
            }
        }

        return $this;
    }
}
